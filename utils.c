//
// Created by Loikeid on 2021/8/24.
//

#include "utils.h"
#include <stdlib.h>
#include <ctype.h>

int hash(const void * source){
    char * ptr = (char *)source;
    int h = 0, g = 0;
    while(*ptr){
        h = 33 * h + *ptr;
        ptr++;
    }
    return h;
}

char * upper(const char * str){
    char * ltr = (char *) calloc (6, sizeof(char));
    unsigned int i = 0;
    while (*(str + i)) {
        *(ltr + i)=(char)toupper(*(str + i));
        i++;
    }
    return ltr;
}
char * lower(const char * str){
    char * utr = (char *) calloc (6, sizeof(char));
    unsigned int i = 0;
    while (*(str + i)) {
        *(utr + i)=(char)tolower(*(str + i));
        i++;
    }
    return utr;
}

int htr2i(const char * hex_str){
    int ret = (int) strtol(hex_str, NULL, 2);
    return ret;
}
int dtr2i(const char * dec_str) {
    int ret = (int) strtol(dec_str, NULL, 2);
    return ret;
}
int btr2i(const char * bin_str) {
    int ret = (int) strtol(bin_str, NULL, 2);
    return ret;
}
int str2i(const char * str){
    int ret = (int) strtol(str+2, NULL, num_base(str));
    return ret;
}

int num_base(const char * str) {
    char *ptr = upper(str);
    int base = 0;
    if(isdigit(*str)&& isdigit(*(str+1)))base = 10;
    if(*(str+1)=='B'||*str=='0')base = 2;
    if(*(str+1)=='X'||*str=='0')base = 16;
    for(int i=2;*(str+i);i++){
        if(base==2&&*(str+i)!='0'&&*(str+i)!='1') return 0;
        if(base==10&&!isdigit(*(str+i))) return 0;
        if(base==16&&!(isalnum(*(str+i))&&*(str+i)<='F'))return 0;
    }
    return base;
}
