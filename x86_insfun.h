//
// Created by Loikeid on 2021/8/26.
//

#ifndef ASMVM_X86_INSFUN_H
#define ASMVM_X86_INSFUN_H

#include "vm.h"

void ADC(uint * target, const uint * source, uint length, x86_reg reg);
void ADD(uint * target, const uint * source, uint length, x86_reg reg);
void AND(uint * target, const uint * source, uint length, x86_reg reg);
void CALL(uint * target, uint length, x86_reg reg);
void CLC(x86_reg reg);
void CLD(x86_reg reg);
void CLI(x86_reg reg);
void CMP(const uint * target, const uint * source, uint length, x86_reg reg);
void DEC(uint * target, uint length, x86_reg reg);
void DIV(const uint * source, uint length, x86_reg reg);
void IDIV(uint * target, const uint * source, uint length, x86_reg reg);
void IMUL(uint * target, const uint * source, uint length, x86_reg reg);
void IN(uint * target, uint * source, uint length, x86_reg reg);
void INC(uint * target, uint length, x86_reg reg);
void INT(uint * target, uint length, x86_reg reg);
void IRET(uint * target, const uint * source, uint length, x86_reg reg);
void JMP(const uint * target, uint length, x86_reg reg);
void LOOP(const uint * target, uint length, x86_reg reg);
void MOV(uint * target, const uint * source, uint length, x86_reg reg);
void MUL(const uint * source, uint length, x86_reg reg);
void NEG(uint * target, uint length, x86_reg reg);
void NOP(x86_reg reg);
void NOT(uint * target, uint length, x86_reg reg);
void OR(uint * target, uint * source, uint length, x86_reg reg);
void OUT(uint * target, uint * source, uint length, x86_reg reg);
void POP(uint * target, uint length, x86_reg reg);
void PUSH(const uint * source, uint length, x86_reg reg);
void RET(x86_reg reg);
void SHL(uint * target, const uint * source, uint length, x86_reg reg);
void SHR(uint * target, const uint * source, uint length, x86_reg reg);
void STC(x86_reg reg);
void STD(x86_reg reg);
void STI(x86_reg reg);
void SUB(uint * target, const uint * source, uint length, x86_reg reg);
void XOR(uint * target, const uint * source, uint length, x86_reg reg);
//    void CMPSB(uint * target, uint * source, uint n_bits, x86_reg reg);
//    void CMPSW(uint * target, uint * source, uint n_bits, x86_reg reg);
//    void MOVSB(uint * target, uint * source, x86_reg reg);
//    void MOVSW(uint * target, uint * source, x86_reg reg);
//    void STOSB(uint * target, uint * source, uint n_bits, x86_reg reg);
//    void STOSW(uint * target, uint * source, uint n_bits, x86_reg reg);

#endif //ASMVM_X86_INSFUN_H
