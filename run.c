//
// Created by Loikeid on 2021/9/19.
//

#include "run.h"

void run(x86_cmd cmd, x86_vm vm){
    switch (cmd->op_obj_count) {
        case 0: ((ex_ins) cmd->ins) (vm->cpu->reg); break;
        case 1: ((pn_ins) cmd->ins) (cmd->op_objs[0], cmd->n_bits, vm->cpu->reg );break;
        case 2: {
            ((tsn_ins) cmd->ins)(
                    cmd->op_objs[0],
                    cmd->op_objs[1],
                    cmd->n_bits,
                    vm->cpu->reg
            );
            break;
        }
    }
}