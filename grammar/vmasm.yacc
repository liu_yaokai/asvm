%{

%}

%token NUM_10 NUM_16 NUM_2 NUM_8
%token SQS SQE BLS BLE PAS PAE
%token INS REG
%token COL CAL
%token EOF EOL
%token KET KES

%%
num:
    | NUM_10    {$$ = $1;}
    | NUM_16    {$$ = $1;}
    | NUM_8     {$$ = $1;}
    | NUM_2     {$$ = $1;}
    ;

sentence:
    | EOL                           {$$ = Sentence(NULL, NULL, NULL);}
    | operate EOL                   {$$ = Sentence($1, NULL, NULL);}
    | operate target EOL            {$$ = Sentence($1, NULL, $2);}
    | operate source target EOL     {$$ = Sentence($1, $2, $3);}
    | source operate target EOL     {$$ = Sentence($2, $1, $3);}
    ;

operate:
    | INS       {$$ = Operation($1);}
    ;

literal_value:
    | num   {$$ = $1;}
    | REG   {$$ = $1;}
    ;

mem_address:
    | SQS literal_value COL literal_value SQE   {$$ = Address($1, $3);}
    ;

source:
    | REG               {$$ = Register($1);}
    | KES REG           {$$ = Register($2);}
    | mem_address       {$$ = $1;}
    | KES mem_address   {$$ = $2;}
    ;

target:
    | REG               {$$ = Register($1);}
    | KET REG           {$$ = Register($2);}
    | mem_address       {$$ = $1;}
    | KET mem_address   {$$ = $2;}
    ;

sentence_list:
    | sentence_list sentence   {$$ = $1.append($2);}
    | sentence                 {$$ = SentenceList(); $$.append($1);}
    ;

block:
    | BLS BLE                   {$$ = Block(NULL);}
    | BLS sentence_list BLE     {$$ = Block($2);}
    ;



%%