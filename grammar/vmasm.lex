%{
#include<stdio.h>
#include "asm.tab.h"
%}
%%
^[^_][0-9_]+[^_]$                           {yylval=atol(yytext); return NUM_10;}
^[^_]0[bB][01][01_]*[^_]$                   {yylval=atol(yytext); return NUM_2;}
^[^_]0[oO][0-7][0-7_]*[^_]$                 {yylval=atol(yytext); return NUM_8;}
^[^_]0[xX][0-9A-Fa-f][0-9A-Fa-f_]*[^_]$     {yylval=atol(yytext); return NUM_16;}

into|to                                     {return KET; /* keywords to emphasize the target. */}
from                                        {return KES; /* keywords to emphasize the source. */}

${REGISTER}
${INSTRUCTION}

//.*                                      {return ; /* single line comment. */}
/\*.*[\n]\*/$                             {return ; /* multi line comment. */}
[ \t\n]+                                    {return ;}

:                                           {return COL;}
@                                           {return CAL;}

;                                           {return EOL;}

\[                                          {return SQS;}
\]                                          {return SQE;}
\{                                          {return BLS;}
\}                                          {return BLE;}
\(                                          {return PAS;}
\)                                          {return PAE;}

%%