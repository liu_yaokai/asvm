//
// Created by Loikeid on 2021/8/24.
//

#ifndef ASMVM_VM_H
#define ASMVM_VM_H

/*
 *      virtual machine
 *
 * CPU :
 *      registers, inset(instruction set)
 * Memory
 * Input/Output :
 *      vhd(virtual hard disk)
 *      Graphic:
 *          Character
 */


// unsigned int
typedef unsigned int uint;

typedef enum {
    // no bits
    No_bits = 0x0,

    // low-eight bits, 8 bits
    Low_bits = 0xff,

    // high eight bits, 8 bits
    High_bits = 0xff00,

    // normal bits, default bits, 16 bits
    Normal_bits = 0xffff,

    // external bits, 32 bits
    External_bits = 0xffffffff
} N_bits;


// ========================  registers  ===========================//

typedef struct _reg_x86 {
    // universal registers
    uint eax;
    uint ebx;
    uint ecx;
    uint edx;

    // index registers
    uint esi;
    uint edi;

    // pointer registers
    uint esp;
    uint ebp;

    // instruction pointer registers
    uint eip;

    // segment registers
    uint *cs;
    uint *ds;
    uint *es;
    uint *ss;
    uint *fs;
    uint *gs;

    // flag register
    //    CF = fr & 0b 0000 0000 0000 0001;
    //    PF = fr & 0b 0000 0000 0000 0100;
    //    AF = fr & 0b 0000 0000 0001 0000;
    //    ZF = fr & 0b 0000 0000 0100 0000;
    //    SF = fr & 0b 0000 0000 1000 0000;
    //    TF = fr & 0b 0000 0001 0000 0000;
    //    IF = fr & 0b 0000 0010 0000 0000;
    //    DF = fr & 0b 0000 0100 0000 0000;
    //    OF = fr & 0b 0000 1000 0000 0000;
    uint fr;

    uint imm; // immediate register
} * x86_reg;

x86_reg x86Reg();

void freeReg(x86_reg reg);

uint * get_reg(const char * name, x86_reg reg_table);


// ====================== instruction set =========================//

// target-source-n_bits  instruction
// the instruction need one target, one source,
// one n_bits register, and the cpu->reg as arguments.
// for examples:
//
//      MOV ESI, EAX    ; target is ESI, source is EAX, n_bits is External_bits
//      ADD BH, AH      ; target is BH, source is AH, n_bits is High_bits
//
// and both of them require the cpu->reg.
//
typedef void (*tsn_ins)(uint *, uint *, uint, x86_reg);


// ptr-n_bits instruction
// the instruction need one target or source,
// one n_bits register, and the cpu->reg as arguments.
// for examples:
//
//      DEC EAX     ; target is AX, n_bits is External_bits
//      DIV CL     ; source is CL (and target is AL), n_bits is Low_bits
//
// and both of them require the cpu->reg.
//
typedef void (*pn_ins)(uint *, uint, x86_reg);


// External Instruction
// the instruction need no argument but cpu->reg.
// for example:
//
//       CLC    ; clean Carry Flag (CF)
//       CLD    ; clean Direction Flag (DF)
//
typedef void (*ex_ins)(x86_reg);


typedef struct _inset_x86 {
    void (*ADC)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*ADD)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*AND)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*CALL)(uint *target, uint length, x86_reg reg);

    void (*CLC)(x86_reg reg);

    void (*CLD)(x86_reg reg);

    void (*CLI)(x86_reg reg);

    void (*CMP)(const uint *target, const uint *source, uint length, x86_reg reg);

    void (*DEC)(uint *target, uint length, x86_reg reg);

    void (*DIV)(const uint *source, uint length, x86_reg reg);

    void (*IDIV)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*IMUL)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*IN)(uint *target, uint *source, uint length, x86_reg reg);

    void (*INC)(uint *target, uint length, x86_reg reg);

    void (*INT)(uint *target, uint length, x86_reg reg);

    void (*IRET)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*JMP)(const uint *target, uint length, x86_reg reg);

    void (*LOOP)(const uint *target, uint length, x86_reg reg);

    void (*MOV)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*MUL)(const uint *source, uint length, x86_reg reg);

    void (*NEG)(uint *target, uint length, x86_reg reg);

    void (*NOP)(x86_reg reg);

    void (*NOT)(uint *target, uint length, x86_reg reg);

    void (*OR)(uint *target, uint *source, uint length, x86_reg reg);

    void (*OUT)(uint *target, uint *source, uint length, x86_reg reg);

    void (*POP)(uint *target, uint length, x86_reg reg);

    void (*PUSH)(const uint *source, uint length, x86_reg reg);

    void (*RET)(x86_reg reg);

    void (*SHL)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*SHR)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*STC)(x86_reg reg);

    void (*STD)(x86_reg reg);

    void (*STI)(x86_reg reg);

    void (*SUB)(uint *target, const uint *source, uint length, x86_reg reg);

    void (*XOR)(uint *target, const uint *source, uint length, x86_reg reg);
    //    void (*CMPSB)(uint * target, uint * source, uint n_bits, x86_reg reg);
    //    void (*CMPSW)(uint * target, uint * source, uint n_bits, x86_reg reg);
    //    void (*MOVSB)(uint * target, uint * source, x86_reg reg);
    //    void (*MOVSW)(uint * target, uint * source, x86_reg reg);
    //    void (*STOSB)(uint * target, uint * source, uint n_bits, x86_reg reg);
    //    void (*STOSW)(uint * target, uint * source, uint n_bits, x86_reg reg);
} *x86_inset;

x86_inset x86Inset();

void freeInset(x86_inset inset);

void *get_ins(const char *name, x86_inset inset_table);


// ============================  CPU  =============================//

typedef struct _cpu_x86 {
    x86_reg reg;
    x86_inset inset;
} *x86_cpu;

x86_cpu x86Cpu(x86_reg reg, x86_inset inset);

void freeCpu(x86_cpu cpu);


// ==========================  memsize  ============================//

typedef struct _mem_x86 {
    uint virtual_start;
    uint size;
    uint data[];
} *x86_mem;

x86_mem x86Mem(unsigned int size, unsigned int virtual_start);

void readMem(x86_mem mem, uint v_address, uint length, void *target);

void writeMem(x86_mem mem, uint v_address, uint length, void *source);

void freeMem(x86_mem mem);

// ========================= Input/Output =========================//


//


typedef struct _vm_x86 {
    x86_cpu cpu;
    x86_mem mem;
} * x86_vm;

#endif //ASMVM_VM_H
