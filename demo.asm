;
; 0x500 to 0x7bff is usable
loader_base_address equ 0x0900
loader_start_sector equ 0x0002

;; MBR code

section mbr vstart = 0x7c00
    ; init registers
    xor ax, ax
    mov ds, ax
    mov es, ax
    cli         ; clear interrupt flags
    mov ss, ax
    mov fs, ax
    sti         ; set interrupt flags

    ; use vga memories
    mov ax, 0xb800
    mov gs, ax

    ; clear screen
    mov ax, 0x0600
    mov bx, 0x0700
    mov cx, 0x0000
    mov dx, 0x184f
    int 0x10

    mov byte [gs:0x0000], 'M'
    mov byte [gs:0x0001], 0x4a
    mov byte [gs:0x0002], 'B'
    mov byte [gs:0x0003], 0x4a
    mov byte [gs:0x0004], 'R'
    mov byte [gs:0x0005], 0x4a


    ; load the loader
    mov eax, loader_start_sector
    mov bx,  loader_base_address
    mov cx,  0x0001
    call read_disk

    jmp loader_base_address

    read_disk:
        ; registers:
        ; eax: LBA number of sector to read
        ; bx : distenation in memory
        ; cx : count of sectors to read

        push bx
        push cx
        push eax

        mov dx, 0x01f2
        mov al, cl
        out dx, al
        pop eax

        mov dx, 0x1f3
        mov cl, 8
        _loop:
            out dx, al
            shr eax, cl
            inc dx
            cmp dx, 0x01f6
            jl _loop

        and al, 0x0f ; 0x0f = 00001111 b
        or  al, 0xe0 ; 0xe0 = 11100000 b
        inc dx
        out dx, al

        inc dx
        mov al, 0x20 ; 0x20 = 00100000 b
        out dx, al

        .not_ready:
            nop
            in  al, dx
            and al, 0x88 ; 0x88 = 10001000 b
            cmp ax, 0x08
            jnz .not_ready

        pop  cx
        imul cx, 0x0100
        mov  dx, 0x01f0

        pop bx
        .read:
            in  ax, dx
            mov [bx], ax
            add bx, 2
            loop .read
        mov ax, bx
        ret

    times 510 -($ - $$) db 0
    db 0x55, 0xaa

    ; final result of registers
    ;
    ; ax = bx = loader_base_address = 0x0900 + 2 * 0x0100 = 0x0b00
    ; bx = ax = loader_base-address = 0x0900 + 2 * 0x0100 = 0x0b00
    ; cx = 0x0001 * 0x0100 =          0x0100
    ; dx =                            0x01f0
    ; ds =                            0x0000
    ; es =                            0x0000
    ; gs =                            0xb800
    ; ss =                            0x0000
    ; fs =                            0x0000
    ;
    ; other registers has not changed.





