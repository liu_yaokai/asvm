//
// Created by Loikeid on 2021/8/24.
//

#ifndef ASMVM_UTILS_H
#define ASMVM_UTILS_H

int htr2i(const char * hex_str);
int dtr2i(const char * dec_str);
int btr2i(const char * bin_str);
int str2i(const char * str);
int num_base(const char * str);

int hash(const void * source);

char * upper(const char * str);
char * lower(const char * str);


#endif //ASMVM_UTILS_H
