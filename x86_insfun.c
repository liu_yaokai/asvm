//
// Created by Loikeid on 2021/8/26.
//

#include "x86_insfun.h"


void UPDATE_FLAG(x86_reg reg, const uint *target, uint length) {
    uint temp = *target & length;
    temp << 1 == (temp << 1 >> 1) ? (reg->fr |= 0x80) : (reg->fr &= ~0x80); // sf
    (temp) ? (reg->fr &= ~0x40) : (reg->fr |= 0x40); // zf
    temp &= 0xff;
    uint pf = 0;
    while (temp) {
        pf += temp & 0x1;
        temp >>= 1;
    }
    pf &= 1;
    pf = ~pf;
    pf ? (reg->fr |= 0x04) : (reg->fr &= ~0x04);
}

void ADC(uint *target, const uint *source, uint length, x86_reg reg) {

}

void ADD(uint *target, const uint *source, uint length, x86_reg reg) {
    unsigned long long temp = *target & length;

    temp += *source & length;
    (temp > length) ? (reg->fr |= 0x01) : (reg->fr &= ~0x01); // (temp>n_bits)?CF=1:CF=0;
    (temp > (length>>1)) ? (reg->fr |= 0x800) : (reg->fr &= ~0x800); // (temp>lengt)?OF=1:OF=0;

    *target &= (0xffffffff - length);
    *target += temp & length;

    UPDATE_FLAG(reg, target, length);
}

void AND(uint *target, const uint *source, uint length, x86_reg reg) {
    unsigned long long temp = *target & length;

    temp &= *source & length;
    (temp > length) ? (reg->fr |= 0x01) : (reg->fr &= ~0x01);

    *target &= ~length;
    *target += temp & length;
    UPDATE_FLAG(reg, target, length);
}

void CALL(uint *target, uint length, x86_reg reg) {

}

void CLC(x86_reg reg) {
    reg->fr &= ~0b0000000000000001;
}

void CLD(x86_reg reg) {
    reg->fr &= ~0b0000010000000000;
}

void CLI(x86_reg reg) {
    reg->fr &= ~0b0000001000000000;
}

void CMP(const uint *target, const uint *source, uint length, x86_reg reg) {
    uint temp = *target & length;

    temp -= *source & length;

    UPDATE_FLAG(reg, &temp, length);
}

//    void CMPSB(uint * target, uint * source, uint n_bits);
//    void CMPSW(uint * target, uint * source, uint n_bits);
void DEC(uint *target, uint length, x86_reg reg) {
    uint temp = (*target) ? *target - 1 : length;
    *target = *target & ~length + temp & length;
    UPDATE_FLAG(reg, target, length);
}

void DIV(const uint *source, uint length, x86_reg reg) {
    uint temp = reg->eax & length;
    temp /= *source & length;
    reg->eax &= ~length;
    reg->eax += temp & length;
    UPDATE_FLAG(reg, &(reg->eax), length);
}

void IDIV(uint *target, const uint *source, uint length, x86_reg reg) {
    uint temp = *target & length;
    temp /= *source & length;
    *target &= ~length;
    *target += temp & length;
    UPDATE_FLAG(reg, target, length);
}

void IMUL(uint *target, const uint *source, uint length, x86_reg reg) {

    unsigned long long temp = *target & length;

    temp *= *source & length;

    (temp > length) ? (reg->fr |= 0x01) : (reg->fr &= ~0x01); // (temp>n_bits)?CF=1:CF=0;
    (temp > (length>>1)) ? (reg->fr |= 0x800) : (reg->fr &= ~0x800); // (temp>lengt)?OF=1:OF=0;

    *target &= ~length;
    *target += temp & length;

    UPDATE_FLAG(reg, target, length);
}

void IN(uint *target, uint *source, uint length, x86_reg reg) {

}

void INC(uint *target, uint length, x86_reg reg) {

    uint temp = *target & length;

    temp = (temp == length) ? 0 : temp + 1;

    *target = (*target & ~length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void INT(uint *target, uint length, x86_reg reg) {

}

void IRET(uint *target, const uint *source, uint length, x86_reg reg) {

}

void JMP(const uint *target, uint length, x86_reg reg) {
    reg->eip = *target & length;
}

void LOOP(const uint *target, uint length, x86_reg reg) {
    if (reg->ecx & length) {
        reg->ecx--;
        reg->eip = *target & length;
    }
}

void MOV(uint *target, const uint *source, uint length, x86_reg reg) {
    *target = (*target & ~length) + (*source & length);
}

//    void MOVSB(uint * target, uint * source, x86_reg reg);
//    void MOVSW(uint * target, uint * source, x86_reg reg);
void MUL(const uint *source, uint length, x86_reg reg) {
    unsigned long long temp = reg->eax & length;

    temp *= *source & length;

    (temp > length) ? (reg->fr |= 0x01) : (reg->fr &= ~0x01); // (temp>n_bits)?CF=1:CF=0;
    (temp > (length>>1)) ? (reg->fr |= 0x800) : (reg->fr &= ~0x800); // (temp>lengt)?OF=1:OF=0;

    reg->eax &= ~length;
    reg->eax += temp;

    UPDATE_FLAG(reg, &(reg->eax), length);
}

void NEG(uint *target, uint length, x86_reg reg) {
    uint temp = *target & length;
    temp *= -1;
    *target = (*target & ~length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void NOP(x86_reg reg) {
    for (int i = 0; i < 512; i++);
}

void NOT(uint *target, uint length, x86_reg reg) {
    uint temp = *target & length;

    temp = ~temp;

    *target = (*target & ~length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void OR(uint *target, uint *source, uint length, x86_reg reg) {
    uint temp = *target & length;

    temp = ~temp;

    *target = (*target & ~length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void OUT(uint *target, uint *source, uint length, x86_reg reg) {

}

void POP(uint *target, uint length, x86_reg reg) {
    unsigned long long temp = *(uint *)(reg->ss+(reg->esp++));
    *target = (*target & ~length) + (temp & length);
}

void PUSH(const uint *source, uint length, x86_reg reg) {
    *(uint *)(reg->ss+(reg->esp--)) = *source & length;
}

void RET(x86_reg reg) {

}

void SHL(uint *target, const uint *source, uint length, x86_reg reg) {
    uint temp = *target<<*source;
    *target = (*target & ~ length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void SHR(uint *target, const uint *source, uint length, x86_reg reg) {
    uint temp = *target>>*source;
    *target = (*target & ~ length) + (temp & length);

    UPDATE_FLAG(reg, target, length);
}

void STC(x86_reg reg) {
    reg->fr |= 0b0000000000000001;
}

void STD(x86_reg reg) {
    reg->fr |= 0b0000010000000000;
}

void STI(x86_reg reg) {
    reg->fr |= 0b0000001000000000;
}

//    void STOSB(uint * target, uint * source, uint n_bits, x86_reg reg);
//    void STOSW(uint * target, uint * source, uint n_bits, x86_reg reg);
void SUB(uint *target, const uint *source, uint length, x86_reg reg) {
    uint temp = *target & length;

    (temp < *source) ? (reg->fr |= 0x01) : (reg->fr &= ~0x01); // (temp<*source)?CF=1:CF=0;
    temp -= *source & length;

    *target &= ~ length;
    *target += temp & length;

    UPDATE_FLAG(reg, target, length);
}

void XOR(uint *target,const uint *source, uint length, x86_reg reg) {
    uint temp = *target & length;

    temp ^= *source & length;

    *target &= ~ length;
    *target += temp & length;

    UPDATE_FLAG(reg, target, length);
}