//
// Created by Loikeid on 2021/9/19.
//

#ifndef ASMVM_PARSE_H
#define ASMVM_PARSE_H
#include "vm.h"

typedef struct _cmd_x86 {
    // row number in source
    uint row;

    // address in virtual memsize
    uint virtual_address;

    uint n_bits;

    // instruction used
    void * ins;

    // operation objects
    uint op_obj_count;
    uint * op_objs[5];

} * x86_cmd;

/*
 * argument explain:
 *
 *  cmdline:
 *      The source command line, aka instruction. such as
 *      "MOV BX, AX" or "INC EBX".
 *
 *  row:
 *      The row number of the cmdline.
 *
 *  cmd:
 *      A cmd object that defined above, aka the Token.
 *      Here cmd will be frequently used, so let it be a container instead of
 *      a temp object will have a better performance.
 *
 *  vm:
 *      The virtual machine object, include CPU and RAM.
 *
 */
void parse(const char * cmdline, int row, x86_cmd cmd, x86_vm vm);

#endif //ASMVM_PARSE_H
