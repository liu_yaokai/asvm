//
// Created by Loikeid on 2021/8/24.
//

#ifndef ASMVM_IO_H
#define ASMVM_IO_H

#ifndef MAX_BYTE_PER_LINE
#define MAX_BYTE_PER_LINE 1024
#endif

//void readBytes(const char * filename, int seed, int n_bits);
//void writeBytes(const char * filename, int seed, int n_bits);

//void readLine(const char * filename, const char * content);
//void writeLine(const char * filename, const char * content);

char * readSource(const char * filename);

#endif //ASMVM_IO_H
