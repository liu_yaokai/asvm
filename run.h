//
// Created by Loikeid on 2021/9/19.
//

#ifndef ASMVM_RUN_H
#define ASMVM_RUN_H

#include "vm.h"
#include "prepro.h"
#include "parse.h"

void run(x86_cmd cmd, x86_vm vm);

#endif //ASMVM_RUN_H
