%{
#include<stdio.h>
#include "reg-config.tab.h"
%}
%%
^[0-9]+$                {yylval=atol(yytext); return NUM_10;}
^0[bB][01]+$            {yylval=atol(yytext); return NUM_2;}
^0[oO][0-7]+$           {yylval=atol(yytext); return NUM_8;}
^0[xX][0-9A-Fa-f]+$     {yylval=atol(yytext); return NUM_16;}


:       {return COL;}
-       {return TO;}
/       {return OF;}
;       {return EOL;}

[a-zA-Z_][a-zA-Z0-9_]*      {return IDENT;}
