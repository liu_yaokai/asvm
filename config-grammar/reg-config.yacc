%{
#include<stdio.h>
#include "conf-proc/register.h"
%}

%token NUM_10 NUM_16 NUM_8 NUM_2
%token IDENT
%token COL EOL
%token TO OF

%%

num:
    | NUM_10    {$$ = $1;}
    | NUM_16    {$$ = $1;}
    | NUM_8     {$$ = $1;}
    | NUM_2     {$$ = $1;}
    ;

definition:
    | IDENT COL num TO num OF IDENT EOF     {$$ = regRegister($1, $3, $5, $7);}
    | IDENT COL num EOF                     {$$ = regRegister($1, $3, NULL, NULL);}