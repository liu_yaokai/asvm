#include <stdio.h>
#include <stdlib.h>
#include "vm.h"
#include "IO.h"
#include "prepro.h"

#include "utils.h"
#include "parse.h"
#include "run.h"

const char * my_source = "../demo.asm";
struct {
    char * filename;
    int debug: 1;
    int save: 1;
    int memsize;
    int vstart;
} program_state = {
        NULL, 0, 0,
        1024, 0x0000
};
/* commands like :
        asmvm demo.asm -debug -save -memsize 1024
*/
void args_process(int argc, char * argv[]){

    // args progress
    int count = 1;
    while(count<argc){
        if(argv[count][0]=='-'){
            switch (argv[count][1]) {
                case 'd': program_state.debug = 1; continue;
                case 's': program_state.save = 1; continue;
                case 'v': program_state.vstart = strtol(argv[++count], NULL, 16); continue;
                case 'm': {
                    program_state.memsize = strtol(argv[++count], NULL, 10);continue;
                }
            }
        }else{
            program_state.filename = argv[count];
        }
        count++;
    }
    if (!program_state.filename){
        printf("No file to open!\n");
        program_state.filename = (char *)my_source;
        //        exit(-1);
    }
}

x86_vm init_vm(){
    // init virtual machine
    x86_vm vm = (x86_vm) calloc(1, sizeof(struct _vm_x86));
    vm->cpu = x86Cpu(x86Reg(), x86Inset());
    vm->mem = x86Mem(program_state.memsize, program_state.vstart);
    return vm;
}

void free_vm(x86_vm vm){
    freeCpu(vm->cpu);
    freeMem(vm->mem);
    free(vm);
}

int main(int argc, char * argv[]) {

    // source parser
    x86_vm vm = init_vm();
    x86_cmd cmd = (x86_cmd) calloc(1, sizeof(struct _cmd_x86));

    parse("mov eax, 0x1", 1, cmd, vm);
    run(cmd, vm);
    printf("eax = 0x%x\n", vm->cpu->reg->eax);

    parse("mov ebx, 0x1", 2, cmd, vm);
    run(cmd, vm);
    printf("eax = 0x%x\n", vm->cpu->reg->ebx);

    while(vm->cpu->reg->eax < 0x8fffffff){
        parse("add eax,  ebx", 3, cmd, vm);
        run(cmd, vm);
        parse("mov edx,  eax", 3, cmd, vm);
        run(cmd, vm);
        parse("mov eax,  ebx", 3, cmd, vm);
        run(cmd, vm);
        parse("mov ebx,  edx", 3, cmd, vm);
        run(cmd, vm);
        printf("eax = %u\n", vm->cpu->reg->eax);
    }

    free_vm(vm);
    return 0;
}
