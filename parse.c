//
// Created by Loikeid on 2021/9/19.
//

#include "parse.h"
#include "utils.h"


void parse(const char *cmdline, int row, x86_cmd cmd, x86_vm vm) {
    int i = 0;
    for (; *(cmdline + i) == ' '; i++);
    char *ptr = (char *) cmdline + i;

    //init cmd_name
    char cmd_name[5];
    for (i = 0; i < 5; i++) cmd_name[i] = 0;

    //init cmd->n_bits
    cmd->row = row;
    cmd->n_bits = External_bits;
    for (int j=0; j < cmd->op_obj_count; j++)
        cmd->op_objs[j] = (void *)0;
    cmd->op_obj_count = 0;

    i = 0; //here the "i" means the cmd_name index in the cmdline now

    while (*ptr) {
        // skip white space and comma
        if (*ptr == ' ' || *ptr == ',') {
            ptr++;
            continue;
        }
        {
            // refresh the cmd_name
            int j = 0;
            while (*ptr != ' ' && *ptr != ',' && *ptr) cmd_name[j++] = *ptr++;
            cmd_name[j] = 0;
        }
        if (i && cmd->ins) // if cmd is not the first cmd and cmd->ins exist
        {
            void *temp_reg = get_reg(cmd_name, vm->cpu->reg);
            if (temp_reg) // if temp_reg exists in the cpu->reg
            {
                // since the first cmd is the ins,
                // the i-th cmd is the (i-1)-th reg
                cmd->op_objs[i - 1] = temp_reg;
                // If cmd_name[2], means length of cmd = 3,
                // means it is a [e?x] reg in 16, 32 bits cpu.
                if (cmd_name[2]) cmd->n_bits &= External_bits;
                    // If 'L' or 'l' in cmd_name, it must be the cmd_name[1].
                else if (cmd_name[1] == 'L' || cmd_name[1] == 'l') cmd->n_bits &= Low_bits;
                    // Same as 'H' or 'h'.
                else if (cmd_name[1] == 'H' || cmd_name[1] == 'h') cmd->n_bits &= High_bits;
                    // default situation.
                else cmd->n_bits &= Normal_bits;
            }
            else if (num_base(cmd_name)) // if cmd_name is an immediate number.
            {
                vm->cpu->reg->imm = str2i(cmd_name);
                cmd->op_objs[i - 1] = &(vm->cpu->reg->imm);
            } else // The temp_reg not in the cpu-reg and not an immediate number.
            {
                // TODO: Some other process here, such variable address.
            }
            cmd->op_obj_count++;
        } else // Cmd is the first cmd, in asm, it should be an instruction.
        {
            void *temp_ins = get_ins(cmd_name, vm->cpu->inset);
            if (temp_ins) // if temp_ins in cpu->inset
            {
                cmd->ins = temp_ins;
            } else // the temp_ins not in cpu->inset
            {
                // TODO: Some error process here.
            }
        }
        i++;
    }
}