//
// Created by Loikeid on 2021/8/24.
//

#include "vm.h"
#include <stdlib.h>
#include "utils.h"

x86_reg x86Reg() {
    x86_reg reg1 = (x86_reg) calloc(1, sizeof(struct _reg_x86));
    return reg1;
}

void freeReg(x86_reg reg) {
    free(reg);
}

// get normal register by name
//
uint *get_reg(const char * name, x86_reg reg_table) {
    char *n = upper(name);
    uint *ptr = NULL;
    /*
     * A hash table will be not easier    than a switch, because you have to generate
     * a table, and import registers to it.
     */
    switch (hash(n)) {
        case 0x12e3e : {
            ptr = &(reg_table->eax);
            break;
        }
        case 0x12e5f : {
            ptr = &(reg_table->ebx);
            break;
        }
        case 0x12e80 : {
            ptr = &(reg_table->ecx);
            break;
        }
        case 0x12ea1 : {
            ptr = &(reg_table->edx);
            break;
        }
        case 0x13081 : {
            ptr = &(reg_table->esi);
            break;
        }
        case 0x12e92 : {
            ptr = &(reg_table->edi);
            break;
        }
        case 0x13088 : {
            ptr = &(reg_table->esp);
            break;
        }
        case 0x12e57 : {
            ptr = &(reg_table->ebp);
            break;
        }
        case 0x12f3e : {
            ptr = &(reg_table->eip);
            break;
        }
    }
//    if(ptr==NULL);
    free(n);
    return ptr;
}

// for PUSH and POP
// get segment register by name
//
uint *get_stack(const char * name, x86_reg reg_table) {
    char *n = upper(name);
    uint *ptr = NULL;
    /*
     * A hash table will be not easier than a switch, because you have to generate
     * a table, and import registers to it.
     */
    switch (hash(n)) {
        case 0x008f6 : {
            ptr = reg_table->cs;
            break;
        }
        case 0x00917 : {
            ptr = reg_table->ds;
            break;
        }
        case 0x00938 : {
            ptr = reg_table->es;
            break;
        }
        case 0x00b06 : {
            ptr = reg_table->ss;
            break;
        }
        case 0x00959 : {
            ptr = reg_table->gs;
            break;
        }
        case 0x0097a : {
            ptr = reg_table->fs;
            break;
        }
    }
    free(n);
    return ptr;
}


#include "x86_insfun.h"

x86_inset x86Inset() {
    x86_inset inset = (x86_inset) calloc(1, sizeof(struct _inset_x86));
    inset->ADC = ADC;
    inset->ADD = ADD;
    inset->AND = AND;
    inset->CALL = CALL;
    inset->CLC = CLC;
    inset->CLD = CLD;
    inset->CLI = CLI;
    inset->CMP = CMP;
    inset->DEC = DEC;
    inset->DIV = DIV;
    inset->IDIV = IDIV;
    inset->IMUL = IMUL;
    inset->IN = IN;
    inset->INC = INC;
    inset->INT = INT;
    inset->IRET = IRET;
    inset->JMP = JMP;
    inset->LOOP = LOOP;
    inset->MOV = MOV;
    inset->MUL = MUL;
    inset->NEG = NEG;
    inset->NOP = NOP;
    inset->NOT = NOT;
    inset->OR = OR;
    inset->OUT = OUT;
    inset->POP = POP;
    inset->PUSH = PUSH;
    inset->RET = RET;
    inset->SHL = SHL;
    inset->SHR = SHR;
    inset->STC = STC;
    inset->STD = STD;
    inset->STI = STI;
    inset->SUB = SUB;
    inset->XOR = XOR;
    return inset;
}

void freeInset(x86_inset inset) {
    free(inset);
}

// get instruction by name
//
void *get_ins(const char *name, x86_inset inset) {
    char *n = upper(name);
    void *ptr = NULL;
    /*
     * Same as the registers, a hash table will be not easier than switch
     * for instructions, because you have to generate a table, and import
     * instructions to it.
     */
    switch (hash(n)) {
        case 0x11d88: {
            ptr = inset->ADC;
            break;
        }
        case 0x11d89: {
            ptr = inset->ADD;
            break;
        }
        case 0x11ed3: {
            ptr = inset->AND;
            break;
        }
        case 0x25dbfc: {
            ptr = inset->CALL;
            break;
        }
        case 0x12712: {
            ptr = inset->CLC;
            break;
        }
        case 0x12713: {
            ptr = inset->CLD;
            break;
        }
        case 0x12718: {
            ptr = inset->CLI;
            break;
        }
        case 0x12740: {
            ptr = inset->CMP;
            break;
        }
        case 0x12a6c: {
            ptr = inset->DEC;
            break;
        }
        case 0x12b03: {
            ptr = inset->DIV;
            break;
        }
        case 0x2932ac: {
            ptr = inset->IDIV;
            break;
        }
        case 0x295a77: {
            ptr = inset->IMUL;
            break;
        }
        case 0x9b7: {
            ptr = inset->IN;
            break;
        }
        case 0x140da: {
            ptr = inset->INC;
            break;
        }
        case 0x140eb: {
            ptr = inset->INT;
            break;
        }
        case 0x296db4: {
            ptr = inset->IRET;
            break;
        }
        case 0x14507: {
            ptr = inset->JMP;
            break;
        }
        case 0x2b075a: {
            ptr = inset->LOOP;
            break;
        }
        case 0x15212: {
            ptr = inset->MOV;
            break;
        }
        case 0x152ce: {
            ptr = inset->MUL;
            break;
        }
        case 0x154fa: {
            ptr = inset->NEG;
            break;
        }
        case 0x1564d: {
            ptr = inset->NOP;
            break;
        }
        case 0x15651: {
            ptr = inset->NOT;
            break;
        }
        case 0xa81: {
            ptr = inset->OR;
            break;
        }
        case 0x15b58: {
            ptr = inset->OUT;
            break;
        }
        case 0x15ecf: {
            ptr = inset->POP;
            break;
        }
        case 0x2d52e0: {
            ptr = inset->PUSH;
            break;
        }
        case 0x1660b: {
            ptr = inset->RET;
            break;
        }
        case 0x16aa7: {
            ptr = inset->SHL;
            break;
        }
        case 0x16aad: {
            ptr = inset->SHR;
            break;
        }
        case 0x16c2a: {
            ptr = inset->STC;
            break;
        }
        case 0x16c2b: {
            ptr = inset->STD;
            break;
        }
        case 0x16c30: {
            ptr = inset->STI;
            break;
        }
        case 0x16c4a: {
            ptr = inset->SUB;
            break;
        }
    }
    return ptr;
}


x86_cpu x86Cpu(x86_reg reg, x86_inset inset) {
    x86_cpu cpu = (x86_cpu) calloc(1, sizeof(struct _cpu_x86));
    cpu->reg = reg;
    cpu->inset = inset;
    return cpu;
}

void freeCpu(x86_cpu cpu) {
    freeReg(cpu->reg);
    freeInset(cpu->inset);
    free(cpu);
}

x86_mem x86Mem(unsigned int size, unsigned int virtual_start) {
    x86_mem mem1 = (x86_mem) calloc(1, sizeof(struct _mem_x86) + size * sizeof(uint));
    mem1->virtual_start = virtual_start;
    mem1->size = size;
    return mem1;
}

void freeMem(x86_mem mem) {
    free(mem);
}

// read source in memsize to a reg from v_address to (v_address + n_bits - 1)
void readMem(x86_mem mem, uint v_address, uint length, void * target) {
    uint * ptr = mem->data+(v_address - mem->virtual_start);
    for(int i=0;i<length;i++){
        *((uint *)target+i) = *(ptr+i);
    }
}

// write result in a reg to memsize from v_address to (v_address + n_bits - 1)
void writeMem(x86_mem mem, uint v_address, uint length, void * source) {
    uint * ptr = mem->data+(v_address - mem->virtual_start);
    for(int i=0;i<length;i++){
        *(ptr+i) = *((uint *)source+i);
    }
}
