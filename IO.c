//
// Created by Loikeid on 2021/8/24.
//

#include "IO.h"

#include<stdio.h>
#include <stdlib.h>

char * readSource(const char * filename){
    char * tptr = (char *) calloc(1024, sizeof(char));
    FILE * fp;
    if((fp=fopen(filename, "r"))==NULL){
        perror("文件打开失败：");
        return NULL;
    }
    fread(tptr, 1, 1024, fp);
    fclose(fp);
    return tptr;
}